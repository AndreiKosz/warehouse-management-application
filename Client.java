import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Admin on 22.04.2017.
 */
public class Client{
    private IntegerProperty id;
    private StringProperty name;
    private  StringProperty address;
    private  StringProperty email;
    private IntegerProperty age;

    public Client(int  id,String name,String address,String email,int age){
        this.id=new SimpleIntegerProperty(id);
        this.name=new SimpleStringProperty(name);
        this.address=new SimpleStringProperty(address);
        this.email=new SimpleStringProperty(email);
        this.age=new SimpleIntegerProperty(age);

    }
    public Client(String name,String address,String email,int age){
        this.name=new SimpleStringProperty(name);
        this.address=new SimpleStringProperty(address);
        this.email=new SimpleStringProperty(email);
        this.age=new SimpleIntegerProperty(age);

    }
    public Client(){
        this.id=new SimpleIntegerProperty(0);
        this.name=new SimpleStringProperty("Empty");
        this.address=new SimpleStringProperty("Empty");
        this.email=new SimpleStringProperty("Empty");
        this.age=new SimpleIntegerProperty(0);

    }

    public String getName() {
        return name.get();
    }

    public String getAddress() {
        return address.get();
    }

    public int getId() {
        return id.get();
    }

    public String getEmail() {
        return email.get();
    }

    public int getAge() {
        return age.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public void setAge(int age) {
        this.age.set(age);
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    public StringProperty nameProperty(){
        return name;
    }
    public IntegerProperty idProperty(){
        return id;
    }

    public StringProperty addressProperty(){
        return address;
    }

    public StringProperty emailProperty(){
        return email;

    }

    public IntegerProperty ageProperty(){
        return age;
    }
}
