import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Admin on 02.05.2017.
 */
public class Orders {

    private IntegerProperty id;
    private StringProperty clientName;
    private StringProperty clientEmail;
    private StringProperty clientAddress;
    private StringProperty productName;
    private IntegerProperty orderedQuantity;

    public Orders(int id, String clientName,String clientEmail,String clientAddress,String productName,int orderedQuantity) {
        this.id = new SimpleIntegerProperty(id);
        this.clientName=new SimpleStringProperty(clientName);
        this.clientEmail=new SimpleStringProperty(clientEmail);
        this.clientAddress=new SimpleStringProperty(clientAddress);
        this.productName=new SimpleStringProperty(productName);
        this.orderedQuantity=new SimpleIntegerProperty(orderedQuantity);

    }

    public Orders(String clientName, String clientEmail,String clientAddress,String productName,int orderedQuantity) {
        this.clientName=new SimpleStringProperty(clientName);
        this.clientAddress=new SimpleStringProperty(clientAddress);
        this.clientEmail=new SimpleStringProperty(clientEmail);
        this.productName=new SimpleStringProperty(productName);
        this.orderedQuantity=new SimpleIntegerProperty(orderedQuantity);

    }

    public Orders() {
        this.id = new SimpleIntegerProperty(0);
        this.clientEmail=new SimpleStringProperty("Empty");
        this.clientAddress=new SimpleStringProperty("Empty");
        this.clientName=new SimpleStringProperty("Empty");
        this.productName=new SimpleStringProperty("Empty");
        this.orderedQuantity=new SimpleIntegerProperty(0);
    }

    public int getId() {
        return id.get();
    }

    public int getOrderedQuantity() {
        return orderedQuantity.get();
    }

    public String getClientAddress() {
        return clientAddress.get();
    }

    public String getClientEmail() {
        return clientEmail.get();
    }

    public String getClientName() {
        return clientName.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress.set(clientAddress);
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail.set(clientEmail);
    }

    public void setClientName(String clientName) {
        this.clientName.set(clientName);
    }

    public void setOrderedQuantity(int orderedQuantity) {
        this.orderedQuantity.set(orderedQuantity);
    }

    public String getProductName() {
        return productName.get();
    }

    public void setProductName(String productName) {
        this.productName.set(productName);
    }
}
