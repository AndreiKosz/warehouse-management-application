/**
 * Created by Admin on 27.04.2017.
 */
public class QuantityValidation implements Validators<Product> {
    private static final int minQuantity=0;
    private static final int maxQuantity=100;
    public void validate(Product product) {
        if(product.getQuantity()<minQuantity||product.getQuantity()>maxQuantity){
            throw new IllegalArgumentException("Quantity is not respected");
        }

    }
}
