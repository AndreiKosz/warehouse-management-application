import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Admin on 24.04.2017.
 */
public class ClientBLL {
    private List<Validators<Client>> validators;

    public ClientBLL(){
        validators=new ArrayList<Validators<Client>>();
        validators.add(new EmailValidator());
    }

    public Client findClientById(int id){
        Client client;
        ClientDAO clientDAO=new ClientDAO();
        client=clientDAO.findById(id);
        if(client==null){
            throw new NoSuchElementException("The student with id = "+id+" was not found");
        }
        return client;
    }
    public int insertClient(Client client){
        ClientDAO clientDAO=new ClientDAO();
        for(Validators<Client> v:validators){
            v.validate(client);

        }
        return clientDAO.insert(client);
    }

    public int updateClient(Client client,int id){
        ClientDAO clientDAO=new ClientDAO();
        Client c=clientDAO.findById(id);
        if(c==null){
            throw new NoSuchElementException("The student with id = "+id+" was not found");
        }

        for(Validators<Client>v:validators){
            v.validate(client);
        }
        return clientDAO.update(client,id);

    }

    public void deleteClient(int id){
        ClientDAO clientDAO=new ClientDAO();
        Client c=clientDAO.findById(id);

        if(c==null){
            throw new NoSuchElementException("The student with id = "+id+" was not found");
        }

        clientDAO.delete(id);

    }
}
