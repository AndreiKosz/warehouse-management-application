/**
 * Created by Admin on 14.03.2017.
 */
/**
 * Created by Admin on 13.03.2017.
 */
import java.awt.*;
import java.awt.Label;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.*;
import java.util.Formatter;
import java.util.ResourceBundle;
import java.util.logging.Level;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class Controller  {

    public Controller() {
    }

    private FirstWindowController controller=new FirstWindowController();

    @FXML
    // The reference of inputText will be injected by the FXML loader
    private TextField ID,Name,Address,Email,Age,ProdName,ProdQuantity,ProdId,orderId,productOrderId,clientOrderId,orderQuantity;

    // The reference of outputText will be injected by the FXML loader
    @FXML
    private TextArea log;
    @FXML
    private Label text1;

    // location and resources will be automatically injected by the FXML loader
    @FXML
    private URL location;

    @FXML
    private ResourceBundle resources;
    @FXML
    private TableView<Client> clientTable;
    @FXML
    private TableColumn<Client,Integer> columnId;
    @FXML
    private TableColumn<Client,String> columnName;
    @FXML
    private TableColumn<Client,String> columnAddress;
    @FXML
    private TableColumn<Client,String> columnEmail;
    @FXML
    private TableColumn<Client,Integer> columnAge;

    @FXML
    private TableView<Product> productTable;
    @FXML
    private TableColumn<Product,Integer> colProdId;
    @FXML
    private TableColumn<Product,String> colProdName;
    @FXML
    private TableColumn<Product,Integer> colProdQuantity;

    @FXML
    private TableView<Orders> orderTable;
    @FXML
    private TableColumn<Orders,Integer> colOrdId;
    @FXML
    private TableColumn<Orders,String> colOrdClient;
    @FXML
    private TableColumn<Orders,String> colOrdEmail;
    @FXML
    private TableColumn<Orders,String> colOrdAddress;
    @FXML
    private TableColumn<Orders,String> colOrdProd;
    @FXML
    private TableColumn<Orders,Integer> colOrdQuantity;

    private ObservableList<Client> clientData;


    private ObservableList<Product> productData;
    private ObservableList<Orders> orderData;



    public void openClientWindow()throws IOException{
       controller.showClientWindow();
    }

    public void openProductWindow() throws IOException{
        controller.showProductWindow();
    }

    public void openOrderWindow() throws IOException{
        controller.showOrderWindow();
    }

    public void insertClient(){
        int age;
        String name,address,email;
        name=Name.getText();
        address=Address.getText();
        email=Email.getText();
        age=Integer.parseInt(Age.getText());
        Client client=new Client(name,address,email,age);
        ClientBLL clientBLL=new ClientBLL();
        clientBLL.insertClient(client);
    }

    public void updateClient(){
        int id;
        id=Integer.parseInt(ID.getText());
        ClientBLL clientBLL=new ClientBLL();
        Client client=clientBLL.findClientById(id);
        if(!Name.getText().isEmpty()){
            client.setName(Name.getText());
        }
        if(!Address.getText().isEmpty()){
            client.setAddress(Address.getText());
        }
        if(!Email.getText().isEmpty()){
            client.setEmail(Email.getText());
        }
        if(!Age.getText().isEmpty()){
            client.setAge(Integer.parseInt(Age.getText()));
        }
        clientBLL.updateClient(client,id);

    }

    public void deleteClient(){
        int id;
        id=Integer.parseInt(ID.getText());
        ClientBLL clientBLL=new ClientBLL();
        clientBLL.deleteClient(id);
    }

    public void tableViewClient(){
        Connection connection=ConnectionFactory.getConnection();
        PreparedStatement statement=null;
        ResultSet results=null;

        try{
            statement=connection.prepareStatement("SELECT * FROM client");
            clientData= FXCollections.observableArrayList();
            results=statement.executeQuery();
            while(results.next()){
                clientData.add(new Client(results.getInt(1),results.getString(2),results.getString(3),results.getString(4),results.getInt(5)));
            }
        }
        catch(SQLException e){
            LOGGER.log(Level.SEVERE, "An error occured while trying to close the ResultSet");
        }

        columnId.setCellValueFactory(new PropertyValueFactory<Client, Integer>("id"));
       columnName.setCellValueFactory(new PropertyValueFactory<Client, String>("name"));
        columnAddress.setCellValueFactory(new PropertyValueFactory<Client, String>("address"));
        columnEmail.setCellValueFactory(new PropertyValueFactory<Client, String>("email"));
        columnAge.setCellValueFactory(new PropertyValueFactory<Client, Integer>("age"));

        clientTable.setItems(null);
        clientTable.setItems(clientData);

    }

    public void insertProduct(){
        int quanity;
        String name;
        name=ProdName.getText();
        quanity=Integer.parseInt(ProdQuantity.getText());
        Product prod=new Product(name,quanity);
        ProductBLL prodBLL=new ProductBLL();
        prodBLL.insertProduct(prod);
    }

    public void updateProduct(){
        int quantity,id;
        quantity=Integer.parseInt(ProdQuantity.getText());
        id=Integer.parseInt(ProdId.getText());

        ProductBLL productBLL=new ProductBLL();
        Product prod=productBLL.findProductById(id);
        if(!ProdName.getText().isEmpty()){
            prod.setName(ProdName.getText());
        }
        if(!ProdQuantity.getText().isEmpty()){
            prod.setQuantity(quantity);
        }
        productBLL.updateProduct(prod,id);

    }

    public void deleteProduct(){
        int id;
        id=Integer.parseInt(ProdId.getText());
        ProductBLL prodBLL=new ProductBLL();
        prodBLL.deleteProduct(id);
    }

    public void tableViewProduct(){

        Connection connection=ConnectionFactory.getConnection();
        PreparedStatement statement=null;
        ResultSet results=null;

        try{
            statement=connection.prepareStatement("SELECT * FROM product");
            productData= FXCollections.observableArrayList();
            results=statement.executeQuery();
            while(results.next()){
                productData.add(new Product(results.getInt(1),results.getString(2),results.getInt(3)));
            }
        }
        catch(SQLException e){
            LOGGER.log(Level.SEVERE, "An error occured while trying to close the ResultSet");
        }

        colProdId.setCellValueFactory(new PropertyValueFactory<Product, Integer>("id"));
        colProdName.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));
        colProdQuantity.setCellValueFactory(new PropertyValueFactory<Product, Integer>("quantity"));

        productTable.setItems(null);
        productTable.setItems(productData);

    }

    public void insertOrder(){
        int quantity,clientId,productId;
        String clientName,clientEmail,clientAddress,productName;
        Client c;
        Product p;
        ClientBLL clientBLL=new ClientBLL();
        ProductBLL productBLL=new ProductBLL();
        OrderBLL orderBLL=new OrderBLL();


        productId=Integer.parseInt(productOrderId.getText());
        clientId=Integer.parseInt(clientOrderId.getText());

        c=clientBLL.findClientById(clientId);
        p=productBLL.findProductById(productId);
        quantity=Integer.parseInt(orderQuantity.getText());
        Orders orders=new Orders(c.getName(),c.getEmail(),c.getAddress(),p.getName(),quantity);

        if(p.getQuantity()<quantity)
            throw new IllegalArgumentException("The required quantity is not availabe");

        p.setQuantity(p.getQuantity()-quantity);
        productBLL.updateProduct(p,productId);
        orderBLL.insertOrder(orders);

    }

    public void updateOrder(){
        int quantity,id,clientId,productId;



        id=Integer.parseInt(orderId.getText());

        OrderBLL orderBLL=new OrderBLL();
        ClientBLL clientBLL=new ClientBLL();
        ProductBLL productBLL=new ProductBLL();
       Orders orders=orderBLL.findOrderById(id);

        if(!clientOrderId.getText().isEmpty()){
            clientId=Integer.parseInt(clientOrderId.getText());
            Client c=clientBLL.findClientById(clientId);
            orders.setClientAddress(c.getAddress());
            orders.setClientEmail(c.getEmail());
            orders.setClientName(c.getName());
        }
        if(!productOrderId.getText().isEmpty()){
            productId=Integer.parseInt(productOrderId.getText());
            Product p=productBLL.findProductById(productId);
            orders.setProductName(p.getName());
        }
        if(!orderQuantity.getText().isEmpty()){
            quantity=Integer.parseInt(orderQuantity.getText());
            orders.setOrderedQuantity(quantity);
        }
        orderBLL.updateOrder(orders,id);

    }

    public void deleteOrder(){
        int id;
        id=Integer.parseInt(orderId.getText());
        OrderBLL orderBLL=new OrderBLL();
        orderBLL.deleteOrder(id);
    }

   public void tableViewOrder(){

        Connection connection=ConnectionFactory.getConnection();
        PreparedStatement statement=null;
        ResultSet results=null;

        try{
            statement=connection.prepareStatement("SELECT * FROM orders");
            orderData= FXCollections.observableArrayList();
            results=statement.executeQuery();
            while(results.next()){
                orderData.add(new Orders(results.getInt(1),results.getString(2),results.getString(3),results.getString(4),results.getString(5),results.getInt(6)));
            }
        }
        catch(SQLException e){
            LOGGER.log(Level.SEVERE, "An error occured while trying to close the ResultSet");
        }

        colOrdId.setCellValueFactory(new PropertyValueFactory<Orders, Integer>("id"));
        colOrdClient.setCellValueFactory(new PropertyValueFactory<Orders, String>("clientName"));
        colOrdEmail.setCellValueFactory(new PropertyValueFactory<Orders, String>("clientEmail"));
        colOrdAddress.setCellValueFactory(new PropertyValueFactory<Orders, String>("clientAddress"));
        colOrdProd.setCellValueFactory(new PropertyValueFactory<Orders, String>("productName"));
        colOrdQuantity.setCellValueFactory(new PropertyValueFactory<Orders, Integer>("orderedQuantity"));

        orderTable.setItems(null);
        orderTable.setItems(orderData);

    }

    public void generateBill(){

        int quantity,clientId,productId;
        Client c;
        Product p;
        ClientBLL clientBLL=new ClientBLL();
        ProductBLL productBLL=new ProductBLL();
        Formatter file;

        productId=Integer.parseInt(productOrderId.getText());
        clientId=Integer.parseInt(clientOrderId.getText());

        c=clientBLL.findClientById(clientId);
        p=productBLL.findProductById(productId);
        quantity=Integer.parseInt(orderQuantity.getText());

        try{
            PrintWriter writer = new PrintWriter("Bill.txt", "UTF-8");
            writer.println("Name:"+c.getName());
            writer.println("Address:"+c.getAddress());
            writer.println("Email:"+c.getEmail());
            writer.println("Product"+p.getName());
            writer.println("Quantity:"+quantity);
            writer.close();
        } catch (IOException e) {
            // do something
        }

    }

}




