import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Admin on 23.04.2017.
 */
public class FieldExtraction {

    public static List<Field> getFields(Object obj){
        List<Field> fieldList=new ArrayList<>();
        fieldList.addAll(Arrays.asList(obj.getClass().getDeclaredFields()));
        return fieldList;
    }

}
