import javafx.fxml.FXML;

import java.awt.*;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Admin on 24.04.2017.
 */
public class ClientControllerGUI {
    public ClientControllerGUI(){

    }
    @FXML
    // The reference of inputText will be injected by the FXML loader
    private TextField ID,Name,address,Email,Age;

    // The reference of outputText will be injected by the FXML loader
    @FXML
    private TextArea log;
    @FXML
    private Label text1;

    // location and resources will be automatically injected by the FXML loader
    @FXML
    private URL location;

    @FXML
    private ResourceBundle resources;

}
