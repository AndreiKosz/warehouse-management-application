/**
 * Created by Admin on 24.04.2017.
 */
import java.io.FileInputStream;
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class FirstWindowController extends Application {
private static Stage primaryStage;
        public static void main(String[] args)
        {
            Application.launch(args);
        }

        @Override
        public void start(Stage stage) throws IOException
        {
            // Create the FXMLLoader
            primaryStage=stage;
            FXMLLoader loader = new FXMLLoader();
            // Path to the FXML File
            String fxmlDocPath = "E:/Faculta/Database/src/FirstWindow.fxml";
            FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);

            // Create the Pane and all Details
            AnchorPane root = (AnchorPane) loader.load(fxmlStream);

            // Create the Scene
            Scene scene = new Scene(root);
            // Set the Scene to the Stage
            stage.setScene(scene);
            // Set the Title to the Stage
            stage.setTitle("Simulare Coada");
            // Display the Stage
            stage.show();

        }

        public void showClientWindow() throws IOException{
            FXMLLoader loader = new FXMLLoader();
            String fxmlDocPath = "E:/Faculta/Database/src/ClientWindow.fxml";
            FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
            AnchorPane root = (AnchorPane) loader.load(fxmlStream);
            Scene scene = new Scene(root);
            Stage stage=new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(scene);
            stage.showAndWait();

        }
    public void showProductWindow() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        String fxmlDocPath = "E:/Faculta/Database/src/ProductWindow.fxml";
        FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
        AnchorPane root = (AnchorPane) loader.load(fxmlStream);
        Scene scene = new Scene(root);
        Stage stage=new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(primaryStage);
        stage.setScene(scene);
        stage.showAndWait();

    }

    public void showOrderWindow() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        String fxmlDocPath = "E:/Faculta/Database/src/OrderWindow.fxml";
        FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
        AnchorPane root = (AnchorPane) loader.load(fxmlStream);
        Scene scene = new Scene(root);
        Stage stage=new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(primaryStage);
        stage.setScene(scene);
        stage.showAndWait();

    }

    }

