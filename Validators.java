/**
 * Created by Admin on 24.04.2017.
 */
public interface Validators<T> {

    public void validate(T t);
}
