import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Admin on 27.04.2017.
 */
public class OrderBLL {



    private List<Validators<Product>> validators;

    public OrderBLL(){
    }

    public Orders findOrderById(int id){
        Orders order;
        OrderDAO orderDAO=new OrderDAO();
        order=orderDAO.findById(id);
        if(order==null){
            throw new NoSuchElementException("The order with id = "+id+" was not found");
        }
        return order;
    }
    public int insertOrder(Orders order){
        OrderDAO orderDAO=new OrderDAO();
        return orderDAO.insert(order);
    }

    public int updateOrder(Orders order,int id){
        OrderDAO orderDAO=new OrderDAO();
        Orders c=orderDAO.findById(id);
        if(c==null){
            throw new NoSuchElementException("The order with id = "+id+" was not found");
        }

        return orderDAO.update(order,id);

    }

    public void deleteOrder(int id){
        OrderDAO orderDAO=new OrderDAO();
        Orders c=orderDAO.findById(id);

        if(c==null){
            throw new NoSuchElementException("The order with id = "+id+" was not found");
        }

        orderDAO.delete(id);
    }
}

