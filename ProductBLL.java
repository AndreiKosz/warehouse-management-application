import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Admin on 27.04.2017.
 */
public class ProductBLL {



        private List<Validators<Product>> validators;

        public ProductBLL(){
            validators=new ArrayList<Validators<Product>>();
            validators.add(new QuantityValidation());
        }

        public Product findProductById(int id){
            Product product;
            ProductDAO productDAO=new ProductDAO();
            product=productDAO.findById(id);
            if(product==null){
                throw new NoSuchElementException("The product with id = "+id+" was not found");
            }
            return product;
        }
        public int insertProduct(Product product){
            ProductDAO clientDAO=new ProductDAO();
            for(Validators<Product> v:validators){
                v.validate(product);

            }
            return clientDAO.insert(product);
        }

        public int updateProduct(Product product,int id){
            ProductDAO productDAO=new ProductDAO();
            Product c=productDAO.findById(id);
            if(c==null){
                throw new NoSuchElementException("The product with id = "+id+" was not found");
            }

            return productDAO.update(product,id);

        }

        public void deleteProduct(int id){
            ProductDAO productDAO=new ProductDAO();
            Product c=productDAO.findById(id);

            if(c==null){
                throw new NoSuchElementException("The product with id = "+id+" was not found");
            }

            productDAO.delete(id);

        }
    }

