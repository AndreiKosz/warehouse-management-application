import javax.xml.transform.Result;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.sun.xml.internal.ws.util.StringUtils.capitalize;

/**
 * Created by Admin on 23.04.2017.
 */
public class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private final Class<T> type;
    @SuppressWarnings("unchecked")
    public AbstractDAO(){
        this.type=(Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public String createSelectStatment(String field){
        StringBuilder sb=new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE "+field+" =?");
        return sb.toString();
    }
    public String createInsertStatment(T t){
        List<Field>fieldList;
        int current=1;
        fieldList=FieldExtraction.getFields(t);
        StringBuilder sb=new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append( " (");
        for(Field f:fieldList) {
            if (!(f.getName().equals("id"))) {
                if (current < fieldList.size()-1) {
                    sb.append(f.getName() + ",");
                } else {
                    sb.append(f.getName());
                }
                current++;
            }
        }
        sb.append(") ");
        sb.append("VALUES ");
        sb.append("(");
        for(int i=0;i<fieldList.size()-1;i++){
            if(i+1<fieldList.size()-1){
                sb.append("?,");
            }
            else{
                sb.append("?");
            }

        }
        sb.append(")");

        return sb.toString();
    }

    public String createUpdateStatment(T t){
        List<Field>fieldList;
        int current=1;
        fieldList=FieldExtraction.getFields(t);
        StringBuilder sb=new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append( " SET ");
        for(Field f:fieldList) {
            if (!(f.getName().equals("id"))) {
                if (current < fieldList.size()-1) {
                    sb.append(f.getName() + " = ?,");
                } else {
                    sb.append(f.getName()+" = ?");
                }
                current++;
            }
        }
        sb.append(" WHERE id=?");

        return sb.toString();
    }

    public String createDeleteStatment(){
        StringBuilder sb=new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append(" WHERE id=?");

        return sb.toString();
    }



    public int insert(T t){
        int id=-1;
        Connection connection=ConnectionFactory.getConnection();
        PreparedStatement statement=null;
        ResultSet generatedKeys=null;
        String query=createInsertStatment(t);
        List<Field> fieldList=FieldExtraction.getFields(t);
        try{
            statement=connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            int i=1;
            for(Field f:fieldList){
                if(!(f.getName().equals("id"))) {
                    try {
                        Method method = t.getClass().getMethod("get" + capitalize(f.getName()));
                        statement.setObject(i, method.invoke(t));
                    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SQLException e) {
                        e.printStackTrace();
                    }
                    i++;
                }
            }
            statement.executeUpdate();
            generatedKeys=statement.getGeneratedKeys();
            if(generatedKeys.next()){
                id=generatedKeys.getInt(1);

            }

        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(generatedKeys);
            ConnectionFactory.close(statement);
        }
        return id;
    }
  private List<T> createObjects(ResultSet resultSet){
      List<T> list=new ArrayList<T>();
      try{
      while(resultSet.next()){
          T instance=type.newInstance();
          for(Field field:type.getDeclaredFields()){
              Object value=resultSet.getObject(field.getName());
              PropertyDescriptor propertyDescriptor=new PropertyDescriptor(field.getName(),type);
              Method method=propertyDescriptor.getWriteMethod();
              method.invoke(instance,value);
          }
          list.add(instance);
      }
      }
      catch(IntrospectionException|IllegalAccessException|InvocationTargetException|SQLException|InstantiationException e){
          e.printStackTrace();
      }

      return list;
  }

    public T findById(int id){
        Connection connection =null;
        PreparedStatement statement=null;
        ResultSet results=null;
        String query = createSelectStatment("id");
        try{
            connection=ConnectionFactory.getConnection();
            statement=connection.prepareStatement(query);
            statement.setInt(1,id);
            results=statement.executeQuery();
            return createObjects(results).get(0);
        }
        catch(SQLException e){
            LOGGER.log(Level.WARNING,type.getName()+" findById "+e.getMessage());
        }
        catch (IndexOutOfBoundsException e){
            LOGGER.log(Level.WARNING,"Could not find element with this ID");
        }
        finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(results);
            ConnectionFactory.close(statement);

        }
        return null;
    }

    public int update(T t,int id){
        int fieldNumber=0;
        Connection connection=ConnectionFactory.getConnection();
        PreparedStatement statement=null;
        ResultSet generatedKeys=null;
        String query=createUpdateStatment(t);
        List<Field> fieldList=FieldExtraction.getFields(t);
        try{
            statement=connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            int i=1;
            for(Field f:fieldList){
                fieldNumber++;
                if(!(f.getName().equals("id"))) {
                    try {
                        Method method = t.getClass().getMethod("get" + capitalize(f.getName()));
                        statement.setObject(i, method.invoke(t));
                    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SQLException e) {
                        e.printStackTrace();
                    }
                    i++;
                }
            }
            statement.setInt(fieldNumber,id);

            statement.executeUpdate();
            generatedKeys=statement.getGeneratedKeys();
            if(generatedKeys.next()){
                id=generatedKeys.getInt(1);

            }

        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(generatedKeys);
            ConnectionFactory.close(statement);
        }
        return id;
    }

    public void delete(int id){
        Connection connection=null;
        PreparedStatement statement=null;
        String query=createDeleteStatment();
        try{
            connection=ConnectionFactory.getConnection();
            statement=connection.prepareStatement(query);
            statement.setInt(1,id);
            statement.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        }


    }





}
